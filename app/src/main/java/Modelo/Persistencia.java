package Modelo;

import com.example.p004c2preexamen.Usuarios;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertUsuario(Usuarios usuario);
    public long updateUsuario(Usuarios usuario);
    public void deleteUsuario(int id);


}
