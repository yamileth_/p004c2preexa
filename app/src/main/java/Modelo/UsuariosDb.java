package Modelo;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.p004c2preexamen.Usuarios;

import java.util.ArrayList;
import java.util.List;

public class UsuariosDb implements Persistencia, Proyeccion {

    private Context context;
    private UsuariosDbHelper helper;
    private SQLiteDatabase db;

    public UsuariosDb(Context context, UsuariosDbHelper helper){
        this.context = context;
        this.helper = helper;
    }

    public UsuariosDb(Context context){
        this.context = context;
        this.helper = new UsuariosDbHelper(this.context);
    }

    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertUsuario(Usuarios usuario) {
        ContentValues values = new ContentValues();

        values.put(DefineTabla.Usuarios.COLUMN_NAME_USUARIO, usuario.getUsuario());
        values.put(DefineTabla.Usuarios.COLUMN_NAME_CORREO, usuario.getCorreo());
        values.put(DefineTabla.Usuarios.COLUMN_NAME_CONTRA, usuario.getPassword());

        this.openDataBase();
        long num = db.insert(DefineTabla.Usuarios.TABLE_NAME, null, values);
        return num;
    }

    @Override
    public long updateUsuario(Usuarios usuario) {
        ContentValues values = new ContentValues();

        values.put(DefineTabla.Usuarios.COLUMN_NAME_ID, usuario.getId());
        values.put(DefineTabla.Usuarios.COLUMN_NAME_USUARIO, usuario.getUsuario());
        values.put(DefineTabla.Usuarios.COLUMN_NAME_CORREO, usuario.getCorreo());
        values.put(DefineTabla.Usuarios.COLUMN_NAME_CONTRA, usuario.getPassword());

        this.openDataBase();
        long num = db.update(
                DefineTabla.Usuarios.TABLE_NAME,
                values,
                DefineTabla.Usuarios.COLUMN_NAME_ID + " = " + usuario.getId(),
                null);
        return num;
    }

    @Override
    public void deleteUsuario(int id) {
        this.openDataBase();
        db.delete(
                DefineTabla.Usuarios.TABLE_NAME,
                DefineTabla.Usuarios.COLUMN_NAME_ID + "=?",
                new String[] {String.valueOf(id)}
        );
    }

    @Override
    public Usuarios getUsuario(String correo) {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTabla.Usuarios.TABLE_NAME,
                DefineTabla.Usuarios.REGISTRO,
                DefineTabla.Usuarios.COLUMN_NAME_CORREO + "=?",
                new String[] {correo},
                null, null, null

        );

        if (cursor.moveToFirst()) {
            Usuarios usuario = readUsuario(cursor);
            cursor.close();
            return usuario;
        } else {
            cursor.close();
            return null;
        }
    }

    @Override
    public List<Usuarios> allUsuarios() {
        db = helper.getWritableDatabase();
        Cursor cursor = db.query(
                DefineTabla.Usuarios.TABLE_NAME,
                DefineTabla.Usuarios.REGISTRO,
                null, null, null, null, null
        );
        List<Usuarios> usuarios = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                Usuarios usuario = readUsuario(cursor);
                usuarios.add(usuario);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return usuarios;
    }

    @Override
    public Usuarios readUsuario(Cursor cursor) {
        Usuarios usuario = new Usuarios();

        usuario.setId(cursor.getInt(0));
        usuario.setUsuario(cursor.getString(1));
        usuario.setCorreo(cursor.getString(2));
        usuario.setPassword(cursor.getString(3));

        return usuario;
    }
}