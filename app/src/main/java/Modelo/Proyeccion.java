package Modelo;

import android.database.Cursor;

import com.example.p004c2preexamen.Usuarios;

import java.util.ArrayList;
import java.util.List;

public interface Proyeccion {
    Usuarios getUsuario(String correo);
    public List<Usuarios> allUsuarios();
    public Usuarios readUsuario(Cursor cursor);
}