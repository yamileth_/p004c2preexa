package com.example.p004c2preexamen;

import java.io.Serializable;

public class Usuarios implements Serializable {
    public int id;
    public String usuario;
    public String correo;
    public String password;

    public Usuarios() {
        this.usuario = "";
        this.password = "";
        this.correo = "";
    }

    public Usuarios(int id, String usuario, String correo, String contra){
        this.id = id;
        this.usuario = usuario;
        this.correo = correo;
        this.password = contra;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
