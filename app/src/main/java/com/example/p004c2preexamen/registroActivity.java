package com.example.p004c2preexamen;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import Modelo.UsuariosDb;

public class registroActivity extends AppCompatActivity{
    private EditText txtCorreo2, txtContra2, txtContraRep, txtUsuario;
    private Button btnRegresar, btnRegistro;
    private UsuariosDb usuariosDb;

    private boolean validar() {
        String correo = txtCorreo2.getText().toString();
        String contra = txtContra2.getText().toString();
        String contrarep = txtContraRep.getText().toString();
        String usuario = txtUsuario.getText().toString();

        if (correo.equals("")|| contra.equals("") || contrarep.equals("") || usuario.equals("")) {
            Toast.makeText(registroActivity.this, "Complete todos los campos", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!contra.equals(contrarep)) {
            Toast.makeText(registroActivity.this, "Verifique contraseñas, no coinciden", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        usuariosDb = new UsuariosDb(this);
        txtCorreo2 = findViewById(R.id.txtCorreo2);
        txtContra2 = findViewById(R.id.txtContra2);
        txtContraRep = findViewById(R.id.txtContraRep);
        btnRegistro = findViewById(R.id.btnRegistro);
        btnRegresar = findViewById(R.id.btnRegresar);
        txtUsuario = findViewById(R.id.txtUsuario);



        btnRegistro.setOnClickListener(v -> {
            String correo = txtCorreo2.getText().toString();
            String contra = txtContra2.getText().toString();
            String usuario = txtUsuario.getText().toString();

            Usuarios usuarioExistente = usuariosDb.getUsuario(correo);

            if (validar()) {
                if (usuarioExistente != null) {
                    Toast.makeText(registroActivity.this, "El correo que desea registrar ya existe", Toast.LENGTH_SHORT).show();
                } else {
                    Usuarios nuevoUsuario = new Usuarios();
                    nuevoUsuario.setUsuario(usuario);
                    nuevoUsuario.setCorreo(correo);
                    nuevoUsuario.setPassword(contra);
                    long resultado = usuariosDb.insertUsuario(nuevoUsuario);
                    if (resultado > 0) {
                        Toast.makeText(registroActivity.this, "El registro se ha completado", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(registroActivity.this, "Ocurrio un error, no se ha podido registrar el usuario", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnRegresar.setOnClickListener(v -> {
            finish();
        });
    }
}
