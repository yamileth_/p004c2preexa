package com.example.p004c2preexamen;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import Modelo.UsuariosDb;

public class listActivity extends AppCompatActivity {
    private ListView listViewUsuarios;
    private UsuariosDb usuariosDb;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        listViewUsuarios = findViewById(R.id.listViewUsuarios);
        usuariosDb = new UsuariosDb(this);
        btnRegresar = findViewById(R.id.btnRegresar);


        List<Usuarios> listaUsuarios = usuariosDb.allUsuarios();
        UsuariosAdapter adapter = new UsuariosAdapter(this, listaUsuarios);

        listViewUsuarios.setAdapter(adapter);

        btnRegresar.setOnClickListener(v -> {

            finish();
        });
    }

}
